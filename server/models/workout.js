const mongoose = require('mongoose');

var WorkoutSchema = mongoose.Schema({
    length: Number,
    date: Date
});

module.exports = mongoose.model('workout', WorkoutSchema);