/**
 * Test workout endpoint
 */
const chai = require('chai'); 
const expect = require('chai').expect;
chai.use(require('chai-http'));
const WorkoutModel = require('../models/workout');

var persistedID;

const server = require('../server');
describe('/api/workouts/', () => {
    // Clean document store after tests
    after((done) => {
        WorkoutModel.remove({}, (err) => {
            if (err) console.log(err);
            done();
        });
    });

    it('Should return 200 OK', (done) => {
        chai.request(server)
            .get('/api/workouts/')
            .then((res) => {
                expect(res).to.have.status(200);
                done();
            });
    });

    describe('POST', () => {
        it('Save should succeed (return id)', (done) => {
            chai.request(server)
                .post('/api/workouts/')
                .send({
                    "length":"60",
                    "date":Date.now()
                })
                .then((res) => {
                    expect(res.text).to.not.be.empty;
                    persistedID = JSON.parse(res.text);
                    done();
                });
        });      
    });

    describe('GET', () => {
        it('Should return object for requested ID', (done) => {
            chai.request(server)
                .get('/api/workouts/' + persistedID)
                .then((res) => {
                    const response = JSON.parse(res.text);
                    expect(response[0]._id).to.equal(persistedID);
                    expect(response[0].length).to.equal(60);
                    //expect(res.text).to.equal(testCar);
                    done();
                });
        });
    });
});