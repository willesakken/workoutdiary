const express = require('express');
const router = express.Router();
const WorkoutModel = require('../models/workout');

// Healthcheck endpoint
router.get('/api/workouts/', (req, res) => {
    res.status(200).send();
});

// Create
router.post('/api/workouts/', (req, res) => {
    WorkoutModel.create(req.body, (err, obj) => {
        if (err) {
            console.log(err.message);
            res.status(500).send(err);
        } 
        res.status(201).send(obj._id);
    });
});

// Read
router.get('/api/workouts/:id', (req, res) => {
    const workoutId = req.params.id;
    WorkoutModel.find({_id: workoutId}, (err, doc) => {
        if (err) {
            console.log(err.message);
            res.status(500).send('Error happened during fetching');
        }
        res.status(200).json(doc);
    });
});

module.exports = router;
